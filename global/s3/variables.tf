# S3 Bucket configuration used for TF backend state

variable "tf_state_s3_bucket_prefix" {
  description = "Bucket prefix of s3 bucket"
  default = "tf-state-"
}

variable "tf_state_s3_bucket_region" {
  description = "Bucket region"
  default = "eu-west-1"
}