provider "aws" {
  version = "~> 1.37"
}

# Backend configuration for terraform resources state
terraform {
  backend "s3" {
    key    = "global/s3/terraform.tfstate"
  }
}

resource "aws_s3_bucket" "TerraformState" {
  bucket_prefix   = "${var.tf_state_s3_bucket_prefix}"
  region   = "${var.tf_state_s3_bucket_region}"

#  versioning {
#    enabled = true
#  }

  tags = {
    Usage = "TF_state"
    ManagedBy = "${var.terraform_tag}"
  }

}

output "tf_state_bucket" {
  value = "${aws_s3_bucket.TerraformState.bucket}"
}