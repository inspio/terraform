variable "demo_cluster_name" {
  description = "Name of the cluster for demonstration"
  default = "demo"
}

variable "terraform_tag" {
  description = "Terraform tag value for ManagedBy tag"
  default = "Terraform"
}

locals {
  # Prefix name of pre-created key-pair for vm SSH access"
  cluster_key_prefix = "${var.demo_cluster_name}"
}