variable "demo_cluster_capacity" {
  description = "Number of EC2 instances in Demo cluster"
  default = 1
}

variable "demo_cluster_instance_type" {
  description = "Instance type for Demo Cluster"
  default = "t2.micro"
}

locals {
  # Demo cluster name within the environment
  demo_cluster_fullname = "${var.demo_cluster_name}-${var.env}"
  demo_cluster_sg = "${data.terraform_remote_state.network.demo_cluster_sg}"
  demo_cluster_subnet = "${data.terraform_remote_state.network.demo_cluster_subnet_ids}"
}