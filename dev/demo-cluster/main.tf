provider "aws" {
  version = "~> 1.37"
}

# Backend configuration for terraform resources state
terraform {
  backend "s3" {
    key    = "dev/demo-cluster/terraform.tfstate"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "tf-state-20180921175418765100000001"
    key    = "dev/network/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ami" "ClusterAmi" {
  most_recent = true

  filter {
    name = "name"
    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"
    values = [
      "amazon",
    ]
  }
}

data "aws_iam_policy_document" "EC2AssumeRole" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ClusterRole" {
  name               = "${local.demo_cluster_fullname}"
  assume_role_policy = "${data.aws_iam_policy_document.EC2AssumeRole.json}"
}

module "DemoCluster" {
  source = "https://gitlab.com/ihar/terraform-demo-module"

  cluster_name           = "${local.demo_cluster_fullname}"
  instance_count = "${var.demo_cluster_capacity}"
  
  ami                    = "${data.aws_ami.ClusterAmi.id}"
  instance_type          = "${var.demo_cluster_instance_type}"
  role_name              = "${aws_iam_role.ClusterRole.name}"
  key_name               = "${local.demo_cluster_key}"
  cluster_sg             = "${local.demo_cluster_sg}"
  subnet_id              = "${local.demo_cluster_subnet}"
  env = "${var.env}"

  extra_tags = {
    Name = "${local.demo_cluster_fullname}"
    ManagedBy = "${var.terraform_tag}"
  }
}