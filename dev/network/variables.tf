# Main VPC configuration

variable "main_vpc_prefix" {
	description = "Main VPC prefix name"
  default = "main"
}

variable "main_vpc_cidr" {
  description = "Main VPC CIDR"
  default = "10.0.0.0/16"
}

locals {
  main_vpc_name = "${var.main_vpc_prefix}-${var.env}"
}


# Demo Cluster Configuration

variable "demo_cluster_azs" {
  type = "list" 
  description = "List of azs for demo cluster"
  default = ["eu-west-1a"]
}

variable "demo_cluster_sbn_cidr" {
  type = "list"
  description = "List of public subnets for demo cluster"
  default = ["10.0.100.0/24"]
}

locals {
  demo_cluster_sg_name = "${var.demo_cluster_name}-${var.env}"
}