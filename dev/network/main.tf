provider "aws" {
  version = "~> 1.37"
}

# Backend configuration for terraform resources state
terraform {
  backend "s3" {
    key    = "dev/network/terraform.tfstate"
  }
}

module "MainVPC" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${local.main_vpc_name}"
  cidr = "${var.main_vpc_cidr}"

  azs             = ["${var.demo_cluster_azs}"]
  public_subnets  = ["${var.demo_cluster_sbn_cidr}"]

  tags = {
    Name = "${local.main_vpc_name}"
    Environment = "${var.env}"
    ManagedBy = "${var.terraform_tag}"
  }
}

resource "aws_security_group" "DemoCluster" {
  name = "${local.demo_cluster_sg_name}"
  description = "Security group for DemoCluster"
  vpc_id      = "${module.MainVPC.vpc_id}"

  tags = {
    Name = "${local.demo_cluster_sg_name}"
    Environment = "${var.env}"
    ManagedBy = "${var.terraform_tag}"
  }
}