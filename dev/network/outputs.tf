output "demo_cluster_subnet_ids" {
  value = "${element(module.MainVPC.public_subnets,1)}"
}

output "demo_cluster_sg" {
  value = "${aws_security_group.DemoCluster.id}"
}