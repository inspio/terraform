variable "env" {
  description = "Name of current Environment"
  default = "dev"
}

locals {
  demo_cluster_key = "${local.cluster_key_prefix}"
}